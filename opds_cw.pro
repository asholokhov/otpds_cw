#-------------------------------------------------
#
# Project created by QtCreator 2013-12-09T23:25:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = opds_cw
TEMPLATE = app


SOURCES += main.cpp\
        tmainwindow.cpp

HEADERS  += tmainwindow.h

FORMS    += tmainwindow.ui
