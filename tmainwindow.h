#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <bitset>

using namespace std;

namespace Ui {
    class TMainWindow;
}

class TMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit TMainWindow(QWidget *parent = 0);
    ~TMainWindow();
    
private slots:
    void on_actionExit_triggered();
    void on_actionAbout_triggered();
    void on_actionCode_triggered();
    void on_pushButton_code_clicked();

    void on_pushButton_clicked();

    void on_ex_lineEdit_textChanged(const QString &arg1);

    void on_gx_lineEdit_textChanged(const QString &arg1);

    void on_actionDecode_triggered();

private:
    Ui::TMainWindow *ui;
    QMap <QString, QString> m_code_page;

    int get_distance(QString s1, QString s2) const;
};

#endif // TMAINWINDOW_H
