#include <QApplication>
#include <QTextCodec>

#include "tmainwindow.h"

int main(int argc, char *argv[])
{
    QTextCodec *utf8 = QTextCodec::codecForName("UTF8");
    QTextCodec::setCodecForLocale(utf8);
    QTextCodec::setCodecForCStrings(utf8);
    QTextCodec::setCodecForTr(utf8);

    QApplication a(argc, argv);

    TMainWindow w;
    w.show();
    
    return a.exec();
}
