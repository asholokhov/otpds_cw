#include "tmainwindow.h"
#include "ui_tmainwindow.h"

#include <QMessageBox>

TMainWindow::TMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TMainWindow)
{
    ui->setupUi(this);

    // init code page
    m_code_page["10000"] = "1101010110";
    m_code_page["01000"] = "0110101011";
    m_code_page["00100"] = "1101100010";
    m_code_page["00010"] = "0110110001";
    m_code_page["00001"] = "1101101111";
    m_code_page["11000"] = "1011111101";
    m_code_page["10100"] = "0000110100";
    m_code_page["10010"] = "1011100111";
    m_code_page["10001"] = "0000111001";
    m_code_page["01100"] = "1011001001";
    m_code_page["01010"] = "0000011010";
    m_code_page["01001"] = "1011000100";
    m_code_page["00110"] = "1011010011";
    m_code_page["00101"] = "0000001101";
    m_code_page["00011"] = "1011011110";
    m_code_page["11100"] = "0110011111";
    m_code_page["11010"] = "1101001100";
    m_code_page["11001"] = "0110010010";
    m_code_page["10110"] = "0110000101";
    m_code_page["10101"] = "1101011011";
    m_code_page["10011"] = "0110001000";
    m_code_page["01110"] = "1101111000";
    m_code_page["01101"] = "0110100110";
    m_code_page["01011"] = "1101110101";
    m_code_page["00111"] = "0110111100";
    m_code_page["11110"] = "0000101110";
    m_code_page["11101"] = "1011110000";
    m_code_page["11011"] = "0000100011";
    m_code_page["10111"] = "1011101010";
    m_code_page["01111"] = "0000010111";
    m_code_page["11111"] = "1101000001";
}

TMainWindow::~TMainWindow()
{
    delete ui;
}

int TMainWindow::get_distance(QString s1, QString s2) const {
    if (s1.size() != s2.size())
        return -1;

    int d = 0;
    for (int i = 0; i < s1.size(); i++)
        if (s1[i] != s2[i])
            d++;

    return d;
}

void TMainWindow::on_actionExit_triggered()
{
    close();
}

void TMainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("About"), tr("<h3>OTPDS Course Work</h3>"
                                             "<small>by Artem Sholokhov, 2013</small>"));
}

void TMainWindow::on_actionCode_triggered()
{
    // check for empty send
    if (ui->gx_lineEdit->text() == "00000") {
        ui->statusBar->showMessage(tr("Nothing to send! Fill G(x) polynom."), 5000);
        return;
    }

    // set R(x) and F(x)
    ui->rx_lineEdit->setText(m_code_page[ui->gx_lineEdit->text()]);
    ui->fx_lineEdit->setText(ui->gx_lineEdit->text() + "." + m_code_page[ui->gx_lineEdit->text()]);

    // set H(x) = F(x) (+) E(x)
    QString fx = ui->gx_lineEdit->text() + m_code_page[ui->gx_lineEdit->text()];
    bitset <15> hx =
            bitset<15>(fx.toStdString()) ^ bitset<15>(ui->ex_lineEdit->text().toStdString());

    ui->hx_lineEdit->setText(QString::fromStdString(hx.to_string()));
}

void TMainWindow::on_pushButton_code_clicked()
{
    on_actionCode_triggered();
    ui->pushButton->setEnabled(true);
}

void TMainWindow::on_pushButton_clicked()
{
    on_actionDecode_triggered();
}

void TMainWindow::on_ex_lineEdit_textChanged(const QString &arg1)
{
    if (arg1.size() < 15)
        return;
    on_actionCode_triggered();
}

void TMainWindow::on_gx_lineEdit_textChanged(const QString &arg1)
{
    if (arg1.size() < 5)
        ui->pushButton_code->setEnabled(false);
    else
        ui->pushButton_code->setEnabled(true);
}

void TMainWindow::on_actionDecode_triggered()
{
    if (ui->hx_lineEdit->text().isEmpty())
        return;

    int min = INT_MAX;
    QString hx = ui->hx_lineEdit->text();
    QMap<QString, QString>::iterator min_cc;

    for (QMap<QString, QString>::iterator it = m_code_page.begin(); it != m_code_page.end(); ++it) {
        QString code_combination = it.key() + it.value();
        int d = get_distance(hx, code_combination);

        if (d <= min) {
            min = d;
            min_cc = it;
        }
    }

    QString sended = ui->gx_lineEdit->text() + ui->rx_lineEdit->text();
    if (min == 0) {
        // errors not found
        ui->textBrowser_result->append("Ошибка не обнаружена.");
        ui->textBrowser_result->append("Проверка не обнаруженной ошибки...");

        if (sended != ui->hx_lineEdit->text())
            ui->textBrowser_result->append("Ошибка есть!");
        else
            ui->textBrowser_result->append("Ошибки нет!");

    } else {
        ui->textBrowser_result->append("Обнаружена ошибка при передаче!");
        ui->textBrowser_result->append("Принятая исправленная комбинация:");
        ui->textBrowser_result->append(min_cc.key() + "." + min_cc.value());
        ui->textBrowser_result->append("Ошибки исправлены в разрядах:");

        QString received = min_cc.key() + min_cc.value();
        for (int i = 0; i < sended.size(); i++)
            if (ui->hx_lineEdit->text()[i] != received[i])
                ui->textBrowser_result->append(QString::number(i + 1) + " ");

        //

        if (sended == received)
            ui->textBrowser_result->append("Проверка: исходная и принятая комбинация совпадают.");
        else
            ui->textBrowser_result->append("Проверка: исходная и принятая комбинация не совпадают!");
    }

    ui->textBrowser_result->append("----------------\n");
}
